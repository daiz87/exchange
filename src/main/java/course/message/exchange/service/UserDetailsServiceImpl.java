package course.message.exchange.service;

import course.message.exchange.exception.UserExistsException;
import course.message.exchange.model.User;
import course.message.exchange.persistence.UserDAO;
import course.message.exchange.util.OnlineUsersStorage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        User user = userDAO.getUserByName(name);
        if (user == null) {
            throw new UsernameNotFoundException("User not found.");
        }
        OnlineUsersStorage.putUser(user);
        return user;
    }

    public void registration(String name, String password) throws UserExistsException {
        if (userDAO.checkUsernameUnique(name)) {
            String encryptedPassword = new BCryptPasswordEncoder().encode(password);
            User user = new User();
            user.setUsername(name);
            user.setPassword(encryptedPassword);
            userDAO.saveUser(user);
        } else {
            throw new UserExistsException("User " + name + " already exists");
        }
    }
}
