package course.message.exchange.persistence;

import course.message.exchange.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDAO {

    User getUserByName(String name);

    User saveUser(User user);

    boolean checkUsernameUnique(String name);
}
