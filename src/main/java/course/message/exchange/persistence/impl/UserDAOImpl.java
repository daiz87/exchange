package course.message.exchange.persistence.impl;

import course.message.exchange.model.User;
import course.message.exchange.persistence.UserDAO;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private EntityManager entityManager;

    @Override
    public User getUserByName(String name) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<User> crit = builder.createQuery(User.class);
        Root<User> root = crit.from(User.class);
        crit.where(builder.equal(root.get("username"), name));
        List<User> userList = entityManager.createQuery(crit).getResultList();
        if (CollectionUtils.isEmpty(userList)) {
            return null;
        }
        return userList.get(0);
    }

    @Override
    @Transactional
    public User saveUser(User user) {
        Session session = entityManager.unwrap(Session.class);
        Long pid = (Long)session.save(user);
        user.setPid(pid);
        return user;
    }

    @Override
    @Transactional
    public boolean checkUsernameUnique(String name) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> cq = builder.createQuery(Long.class);
        cq.select(builder.count(cq.from(User.class))).where(builder.equal(cq.from(User.class).get("username"), name));
        Long count = entityManager.createQuery(cq).getSingleResult();
        return count < 1;
    }
}
