package course.message.exchange.controller;

import course.message.exchange.exception.UserExistsException;
import course.message.exchange.model.Message;
import course.message.exchange.model.User;
import course.message.exchange.service.UserDetailsServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Controller
public class MainController {

    @Autowired
    private UserDetailsServiceImpl userService;

    @GetMapping(value = "/")
    public String home() {
        return "home";
    }

    @GetMapping(value = "/login")
    public String login() {
        return "login";
    }

    @GetMapping(value = "/register")
    public String register() {
        return "register";
    }

    @PostMapping(value = "/registration")
    public String registration(@RequestParam String username, @RequestParam String password,
                                     HttpServletResponse response) throws IOException {
        try {
            userService.registration(username, password);
        } catch (UserExistsException e) {
            response.sendRedirect("register?error=true");
        }
        return "login";
    }

    @GetMapping(value = "/chat")
    public ModelAndView chatStart(Map<String, Object> model, HttpServletRequest request) {
        model.put("user", request.getRemoteUser());
        return new ModelAndView("chat");
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/chat")
    public Message sendMessage(@Payload Message chatMessage) {
        return chatMessage;
    }
}
