package course.message.exchange.util;

import course.message.exchange.model.User;

import java.util.HashMap;
import java.util.Map;

public class OnlineUsersStorage {

    private static Map<String, User> storage = new HashMap<>();

    public static User getUser(String username) {
        return storage.get(username);
    }

    public static void putUser(User user) {
        storage.put(user.getUsername(), user);
    }

    public static void deleteUser(Object user) {
        String username = ((User) user).getUsername();
        storage.remove(username);
    }
}
