package course.message.exchange.exception;

public class UserExistsException extends Exception{
    public UserExistsException(String message) {
        super(message);
    }
}
